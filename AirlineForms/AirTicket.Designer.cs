﻿namespace AirlineForms
{
    partial class AirTicket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 =
                new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 =
                new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 =
                new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 =
                new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(AirTicket));
            this.pnl = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.gpPeopleFlying = new System.Windows.Forms.GroupBox();
            this.lblInfants = new System.Windows.Forms.Label();
            this.lblChildren = new System.Windows.Forms.Label();
            this.lblAdults = new System.Windows.Forms.Label();
            this.cmbInfants = new System.Windows.Forms.ComboBox();
            this.cmbAdults = new System.Windows.Forms.ComboBox();
            this.cmbChildren = new System.Windows.Forms.ComboBox();
            this.grpDates = new System.Windows.Forms.GroupBox();
            this.lblDeparture = new System.Windows.Forms.Label();
            this.chbRoundTrip = new System.Windows.Forms.CheckBox();
            this.lblArrival = new System.Windows.Forms.Label();
            this.dateLeave = new System.Windows.Forms.DateTimePicker();
            this.dateReturn = new System.Windows.Forms.DateTimePicker();
            this.lblReturnError = new System.Windows.Forms.Label();
            this.lblLeaveError = new System.Windows.Forms.Label();
            this.grpJourney = new System.Windows.Forms.GroupBox();
            this.cmbDestination = new System.Windows.Forms.ComboBox();
            this.lblCabinError = new System.Windows.Forms.Label();
            this.cmbOrigin = new System.Windows.Forms.ComboBox();
            this.lblDestinationError = new System.Windows.Forms.Label();
            this.lblOriginError = new System.Windows.Forms.Label();
            this.lblCabinClass = new System.Windows.Forms.Label();
            this.cmbCabinClass = new System.Windows.Forms.ComboBox();
            this.lblDestination = new System.Windows.Forms.Label();
            this.lblOrigin = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSortList = new System.Windows.Forms.Button();
            this.lblRateLimit = new System.Windows.Forms.Label();
            this.dgResponse = new System.Windows.Forms.DataGridView();
            this.Logo = new System.Windows.Forms.DataGridViewImageColumn();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Route = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Select = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnSearchFlights = new System.Windows.Forms.Button();
            this.pnl.SuspendLayout();
            this.gpPeopleFlying.SuspendLayout();
            this.grpDates.SuspendLayout();
            this.grpJourney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgResponse)).BeginInit();
            this.SuspendLayout();
            // 
            // pnl
            // 
            this.pnl.Controls.Add(this.btnBack);
            this.pnl.Controls.Add(this.gpPeopleFlying);
            this.pnl.Controls.Add(this.grpDates);
            this.pnl.Controls.Add(this.grpJourney);
            this.pnl.Controls.Add(this.button1);
            this.pnl.Controls.Add(this.btnSortList);
            this.pnl.Controls.Add(this.lblRateLimit);
            this.pnl.Controls.Add(this.dgResponse);
            this.pnl.Controls.Add(this.btnSearchFlights);
            this.pnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl.Location = new System.Drawing.Point(0, 0);
            this.pnl.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.pnl.Name = "pnl";
            this.pnl.Size = new System.Drawing.Size(1188, 1006);
            this.pnl.TabIndex = 13;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(20, 294);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(150, 29);
            this.btnBack.TabIndex = 47;
            this.btnBack.Text = "🠘 Go Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // gpPeopleFlying
            // 
            this.gpPeopleFlying.Controls.Add(this.lblInfants);
            this.gpPeopleFlying.Controls.Add(this.lblChildren);
            this.gpPeopleFlying.Controls.Add(this.lblAdults);
            this.gpPeopleFlying.Controls.Add(this.cmbInfants);
            this.gpPeopleFlying.Controls.Add(this.cmbAdults);
            this.gpPeopleFlying.Controls.Add(this.cmbChildren);
            this.gpPeopleFlying.Location = new System.Drawing.Point(905, 17);
            this.gpPeopleFlying.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gpPeopleFlying.Name = "gpPeopleFlying";
            this.gpPeopleFlying.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.gpPeopleFlying.Size = new System.Drawing.Size(234, 192);
            this.gpPeopleFlying.TabIndex = 46;
            this.gpPeopleFlying.TabStop = false;
            this.gpPeopleFlying.Text = "People Flying";
            // 
            // lblInfants
            // 
            this.lblInfants.AutoSize = true;
            this.lblInfants.Location = new System.Drawing.Point(154, 23);
            this.lblInfants.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInfants.Name = "lblInfants";
            this.lblInfants.Size = new System.Drawing.Size(43, 15);
            this.lblInfants.TabIndex = 40;
            this.lblInfants.Text = "Infants";
            // 
            // lblChildren
            // 
            this.lblChildren.AutoSize = true;
            this.lblChildren.Location = new System.Drawing.Point(83, 23);
            this.lblChildren.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblChildren.Name = "lblChildren";
            this.lblChildren.Size = new System.Drawing.Size(52, 15);
            this.lblChildren.TabIndex = 39;
            this.lblChildren.Text = "Children";
            // 
            // lblAdults
            // 
            this.lblAdults.AutoSize = true;
            this.lblAdults.Location = new System.Drawing.Point(14, 23);
            this.lblAdults.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAdults.Name = "lblAdults";
            this.lblAdults.Size = new System.Drawing.Size(41, 15);
            this.lblAdults.TabIndex = 38;
            this.lblAdults.Text = "Adults";
            // 
            // cmbInfants
            // 
            this.cmbInfants.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbInfants.FormattingEnabled = true;
            this.cmbInfants.Items.AddRange(new object[] {"0", "1", "2", "3", "4", "5", "6", "7", "8"});
            this.cmbInfants.Location = new System.Drawing.Point(155, 53);
            this.cmbInfants.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.cmbInfants.Name = "cmbInfants";
            this.cmbInfants.Size = new System.Drawing.Size(40, 23);
            this.cmbInfants.TabIndex = 23;
            // 
            // cmbAdults
            // 
            this.cmbAdults.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAdults.FormattingEnabled = true;
            this.cmbAdults.Items.AddRange(new object[] {"1", "2", "3", "4", "5", "6", "7", "8"});
            this.cmbAdults.Location = new System.Drawing.Point(15, 53);
            this.cmbAdults.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.cmbAdults.Name = "cmbAdults";
            this.cmbAdults.Size = new System.Drawing.Size(40, 23);
            this.cmbAdults.TabIndex = 22;
            // 
            // cmbChildren
            // 
            this.cmbChildren.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChildren.FormattingEnabled = true;
            this.cmbChildren.Items.AddRange(new object[] {"0", "1", "2", "3", "4", "5", "6", "7", "8"});
            this.cmbChildren.Location = new System.Drawing.Point(84, 53);
            this.cmbChildren.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.cmbChildren.Name = "cmbChildren";
            this.cmbChildren.Size = new System.Drawing.Size(40, 23);
            this.cmbChildren.TabIndex = 21;
            // 
            // grpDates
            // 
            this.grpDates.Controls.Add(this.lblDeparture);
            this.grpDates.Controls.Add(this.chbRoundTrip);
            this.grpDates.Controls.Add(this.lblArrival);
            this.grpDates.Controls.Add(this.dateLeave);
            this.grpDates.Controls.Add(this.dateReturn);
            this.grpDates.Controls.Add(this.lblReturnError);
            this.grpDates.Controls.Add(this.lblLeaveError);
            this.grpDates.Location = new System.Drawing.Point(477, 17);
            this.grpDates.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpDates.Name = "grpDates";
            this.grpDates.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpDates.Size = new System.Drawing.Size(189, 192);
            this.grpDates.TabIndex = 45;
            this.grpDates.TabStop = false;
            this.grpDates.Text = "Dates";
            // 
            // lblDeparture
            // 
            this.lblDeparture.AutoSize = true;
            this.lblDeparture.Location = new System.Drawing.Point(9, 42);
            this.lblDeparture.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDeparture.Name = "lblDeparture";
            this.lblDeparture.Size = new System.Drawing.Size(59, 15);
            this.lblDeparture.TabIndex = 42;
            this.lblDeparture.Text = "Departure";
            // 
            // chbRoundTrip
            // 
            this.chbRoundTrip.AutoSize = true;
            this.chbRoundTrip.Location = new System.Drawing.Point(9, 110);
            this.chbRoundTrip.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chbRoundTrip.Name = "chbRoundTrip";
            this.chbRoundTrip.Size = new System.Drawing.Size(94, 19);
            this.chbRoundTrip.TabIndex = 3;
            this.chbRoundTrip.Text = "Return Flight";
            this.chbRoundTrip.UseVisualStyleBackColor = true;
            this.chbRoundTrip.CheckedChanged += new System.EventHandler(this.chbRoundTrip_CheckedChanged);
            // 
            // lblArrival
            // 
            this.lblArrival.AutoSize = true;
            this.lblArrival.Location = new System.Drawing.Point(202, 42);
            this.lblArrival.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblArrival.Name = "lblArrival";
            this.lblArrival.Size = new System.Drawing.Size(41, 15);
            this.lblArrival.TabIndex = 43;
            this.lblArrival.Text = "Arrival";
            this.lblArrival.Visible = false;
            // 
            // dateLeave
            // 
            this.dateLeave.Location = new System.Drawing.Point(9, 72);
            this.dateLeave.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.dateLeave.Name = "dateLeave";
            this.dateLeave.Size = new System.Drawing.Size(159, 23);
            this.dateLeave.TabIndex = 13;
            this.dateLeave.Value = new System.DateTime(2020, 1, 2, 0, 0, 0, 0);
            this.dateLeave.ValueChanged += new System.EventHandler(this.dateLeave_ValueChanged);
            // 
            // dateReturn
            // 
            this.dateReturn.Location = new System.Drawing.Point(202, 72);
            this.dateReturn.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.dateReturn.Name = "dateReturn";
            this.dateReturn.Size = new System.Drawing.Size(159, 23);
            this.dateReturn.TabIndex = 14;
            this.dateReturn.Value = new System.DateTime(2020, 1, 25, 0, 0, 0, 0);
            this.dateReturn.Visible = false;
            this.dateReturn.ValueChanged += new System.EventHandler(this.dateReturn_ValueChanged);
            // 
            // lblReturnError
            // 
            this.lblReturnError.BackColor = System.Drawing.Color.Transparent;
            this.lblReturnError.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblReturnError.ForeColor = System.Drawing.Color.Red;
            this.lblReturnError.Location = new System.Drawing.Point(252, 42);
            this.lblReturnError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReturnError.Name = "lblReturnError";
            this.lblReturnError.Size = new System.Drawing.Size(136, 27);
            this.lblReturnError.TabIndex = 34;
            this.lblReturnError.Text = "Date is in the past";
            this.lblReturnError.Visible = false;
            // 
            // lblLeaveError
            // 
            this.lblLeaveError.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblLeaveError.ForeColor = System.Drawing.Color.Red;
            this.lblLeaveError.Location = new System.Drawing.Point(77, 42);
            this.lblLeaveError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLeaveError.Name = "lblLeaveError";
            this.lblLeaveError.Size = new System.Drawing.Size(107, 22);
            this.lblLeaveError.TabIndex = 33;
            this.lblLeaveError.Text = "Date is in the past";
            this.lblLeaveError.Visible = false;
            // 
            // grpJourney
            // 
            this.grpJourney.Controls.Add(this.cmbDestination);
            this.grpJourney.Controls.Add(this.lblCabinError);
            this.grpJourney.Controls.Add(this.cmbOrigin);
            this.grpJourney.Controls.Add(this.lblDestinationError);
            this.grpJourney.Controls.Add(this.lblOriginError);
            this.grpJourney.Controls.Add(this.lblCabinClass);
            this.grpJourney.Controls.Add(this.cmbCabinClass);
            this.grpJourney.Controls.Add(this.lblDestination);
            this.grpJourney.Controls.Add(this.lblOrigin);
            this.grpJourney.Location = new System.Drawing.Point(12, 17);
            this.grpJourney.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpJourney.Name = "grpJourney";
            this.grpJourney.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.grpJourney.Size = new System.Drawing.Size(350, 192);
            this.grpJourney.TabIndex = 44;
            this.grpJourney.TabStop = false;
            this.grpJourney.Text = "Route and Cabin Class";
            // 
            // cmbDestination
            // 
            this.cmbDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDestination.FormattingEnabled = true;
            this.cmbDestination.Items.AddRange(new object[] {"DOH", "DXB", "AUH", "CMB", "BOM", "SIN", "CDG"});
            this.cmbDestination.Location = new System.Drawing.Point(170, 75);
            this.cmbDestination.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbDestination.Name = "cmbDestination";
            this.cmbDestination.Size = new System.Drawing.Size(137, 23);
            this.cmbDestination.TabIndex = 48;
            // 
            // lblCabinError
            // 
            this.lblCabinError.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblCabinError.ForeColor = System.Drawing.Color.Red;
            this.lblCabinError.Location = new System.Drawing.Point(247, 123);
            this.lblCabinError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCabinError.Name = "lblCabinError";
            this.lblCabinError.Size = new System.Drawing.Size(80, 15);
            this.lblCabinError.TabIndex = 35;
            this.lblCabinError.Text = "(incorrect)";
            this.lblCabinError.Visible = false;
            // 
            // cmbOrigin
            // 
            this.cmbOrigin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrigin.FormattingEnabled = true;
            this.cmbOrigin.Items.AddRange(new object[] {"SFO", "LHR", "EWR", "JFK", "LAX", "DFW", "HNL"});
            this.cmbOrigin.Location = new System.Drawing.Point(8, 75);
            this.cmbOrigin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbOrigin.Name = "cmbOrigin";
            this.cmbOrigin.Size = new System.Drawing.Size(137, 23);
            this.cmbOrigin.TabIndex = 47;
            this.cmbOrigin.TextChanged += new System.EventHandler(this.cmbOrigin_TextChanged);
            // 
            // lblDestinationError
            // 
            this.lblDestinationError.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblDestinationError.ForeColor = System.Drawing.Color.Red;
            this.lblDestinationError.Location = new System.Drawing.Point(241, 42);
            this.lblDestinationError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDestinationError.Name = "lblDestinationError";
            this.lblDestinationError.Size = new System.Drawing.Size(86, 15);
            this.lblDestinationError.TabIndex = 32;
            this.lblDestinationError.Text = "(incorrect)";
            this.lblDestinationError.Visible = false;
            // 
            // lblOriginError
            // 
            this.lblOriginError.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblOriginError.ForeColor = System.Drawing.Color.Red;
            this.lblOriginError.Location = new System.Drawing.Point(44, 42);
            this.lblOriginError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOriginError.Name = "lblOriginError";
            this.lblOriginError.Size = new System.Drawing.Size(86, 15);
            this.lblOriginError.TabIndex = 31;
            this.lblOriginError.Text = "(incorrect)";
            this.lblOriginError.Visible = false;
            // 
            // lblCabinClass
            // 
            this.lblCabinClass.AutoSize = true;
            this.lblCabinClass.Location = new System.Drawing.Point(170, 123);
            this.lblCabinClass.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblCabinClass.Name = "lblCabinClass";
            this.lblCabinClass.Size = new System.Drawing.Size(68, 15);
            this.lblCabinClass.TabIndex = 25;
            this.lblCabinClass.Text = "Cabin Class";
            // 
            // cmbCabinClass
            // 
            this.cmbCabinClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCabinClass.FormattingEnabled = true;
            this.cmbCabinClass.Items.AddRange(new object[] {"Economy", "Business", "First"});
            this.cmbCabinClass.Location = new System.Drawing.Point(170, 153);
            this.cmbCabinClass.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.cmbCabinClass.Name = "cmbCabinClass";
            this.cmbCabinClass.Size = new System.Drawing.Size(135, 23);
            this.cmbCabinClass.TabIndex = 24;
            this.cmbCabinClass.SelectedIndexChanged += new System.EventHandler(this.cmbCabinClass_SelectedIndexChanged);
            // 
            // lblDestination
            // 
            this.lblDestination.AutoSize = true;
            this.lblDestination.Location = new System.Drawing.Point(170, 42);
            this.lblDestination.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDestination.Name = "lblDestination";
            this.lblDestination.Size = new System.Drawing.Size(67, 15);
            this.lblDestination.TabIndex = 18;
            this.lblDestination.Text = "Destination";
            // 
            // lblOrigin
            // 
            this.lblOrigin.AutoSize = true;
            this.lblOrigin.Location = new System.Drawing.Point(7, 42);
            this.lblOrigin.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblOrigin.Name = "lblOrigin";
            this.lblOrigin.Size = new System.Drawing.Size(40, 15);
            this.lblOrigin.TabIndex = 17;
            this.lblOrigin.Text = "Origin";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(162, 225);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 25);
            this.button1.TabIndex = 41;
            this.button1.Text = "LOCATION";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSortList
            // 
            this.btnSortList.Location = new System.Drawing.Point(989, 294);
            this.btnSortList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSortList.Name = "btnSortList";
            this.btnSortList.Size = new System.Drawing.Size(150, 29);
            this.btnSortList.TabIndex = 37;
            this.btnSortList.Text = "Sort Lowest to Highest";
            this.btnSortList.UseVisualStyleBackColor = true;
            this.btnSortList.Visible = false;
            this.btnSortList.Click += new System.EventHandler(this.btnSortList_Click);
            // 
            // lblRateLimit
            // 
            this.lblRateLimit.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Italic,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblRateLimit.ForeColor = System.Drawing.Color.Red;
            this.lblRateLimit.Location = new System.Drawing.Point(477, 224);
            this.lblRateLimit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRateLimit.Name = "lblRateLimit";
            this.lblRateLimit.Size = new System.Drawing.Size(189, 27);
            this.lblRateLimit.TabIndex = 36;
            this.lblRateLimit.Text = "Rate limit for the API has exceeded";
            this.lblRateLimit.Visible = false;
            // 
            // dgResponse
            // 
            this.dgResponse.AllowUserToAddRows = false;
            this.dgResponse.BackgroundColor = System.Drawing.Color.White;
            this.dgResponse.ColumnHeadersHeightSizeMode =
                System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgResponse.ColumnHeadersVisible = false;
            this.dgResponse.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[]
                {this.Logo, this.Index, this.Route, this.Price, this.Select});
            this.dgResponse.Location = new System.Drawing.Point(0, 338);
            this.dgResponse.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgResponse.Name = "dgResponse";
            this.dgResponse.RowHeadersVisible = false;
            this.dgResponse.RowHeadersWidth = 60;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgResponse.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgResponse.Size = new System.Drawing.Size(1188, 667);
            this.dgResponse.TabIndex = 30;
            this.dgResponse.CellClick +=
                new System.Windows.Forms.DataGridViewCellEventHandler(this.dgResponse_CellClick);
            // 
            // Logo
            // 
            this.Logo.HeaderText = "Logo";
            this.Logo.Name = "Logo";
            this.Logo.ReadOnly = true;
            this.Logo.Width = 254;
            // 
            // Index
            // 
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Visible = false;
            // 
            // Route
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.Route.DefaultCellStyle = dataGridViewCellStyle1;
            this.Route.HeaderText = "Route";
            this.Route.Name = "Route";
            this.Route.ReadOnly = true;
            this.Route.Width = 254;
            // 
            // Price
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.Price.DefaultCellStyle = dataGridViewCellStyle2;
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 254;
            // 
            // Select
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.MediumOrchid;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F,
                System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.Select.DefaultCellStyle = dataGridViewCellStyle3;
            this.Select.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Select.HeaderText = "Select";
            this.Select.Name = "Select";
            this.Select.ReadOnly = true;
            this.Select.Text = "Select Flight";
            this.Select.UseColumnTextForButtonValue = true;
            this.Select.Width = 254;
            // 
            // btnSearchFlights
            // 
            this.btnSearchFlights.Location = new System.Drawing.Point(525, 272);
            this.btnSearchFlights.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSearchFlights.Name = "btnSearchFlights";
            this.btnSearchFlights.Size = new System.Drawing.Size(94, 51);
            this.btnSearchFlights.TabIndex = 29;
            this.btnSearchFlights.Text = "Find Flights";
            this.btnSearchFlights.UseVisualStyleBackColor = true;
            this.btnSearchFlights.Click += new System.EventHandler(this.btnSearchFlights_Click);
            // 
            // AirTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1188, 1006);
            this.Controls.Add(this.pnl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.MaximizeBox = false;
            this.Name = "AirTicket";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Your Flight";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AirTicket_KeyDown);
            this.pnl.ResumeLayout(false);
            this.gpPeopleFlying.ResumeLayout(false);
            this.gpPeopleFlying.PerformLayout();
            this.grpDates.ResumeLayout(false);
            this.grpDates.PerformLayout();
            this.grpJourney.ResumeLayout(false);
            this.grpJourney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dgResponse)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel pnl;
        private System.Windows.Forms.Label lblCabinClass;
        private System.Windows.Forms.DataGridViewImageColumn Logo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn Route;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewButtonColumn Select;
        private System.Windows.Forms.ComboBox cmbCabinClass;
        private System.Windows.Forms.ComboBox cmbInfants;
        private System.Windows.Forms.ComboBox cmbAdults;
        private System.Windows.Forms.ComboBox cmbChildren;
        private System.Windows.Forms.Label lblDestination;
        private System.Windows.Forms.Label lblOrigin;
        private System.Windows.Forms.DateTimePicker dateLeave;
        private System.Windows.Forms.DateTimePicker dateReturn;
        private System.Windows.Forms.CheckBox chbRoundTrip;
        private System.Windows.Forms.Button btnSearchFlights;
        private System.Windows.Forms.DataGridViewImageColumn AirlineLogo;
        private System.Windows.Forms.DataGridViewTextBoxColumn RouteList;
        private System.Windows.Forms.DataGridViewTextBoxColumn FlightPrice;
        private System.Windows.Forms.DataGridViewButtonColumn SelectButton;
        private System.Windows.Forms.DataGridView dgResponse;
        private System.Windows.Forms.Label lblOriginError;
        private System.Windows.Forms.Label lblDestinationError;
        private System.Windows.Forms.Label lblReturnError;
        private System.Windows.Forms.Label lblLeaveError;
        private System.Windows.Forms.Label lblCabinError;
        private System.Windows.Forms.Label lblRateLimit;
        private System.Windows.Forms.Button btnSortList;
        private System.Windows.Forms.Label lblInfants;
        private System.Windows.Forms.Label lblChildren;
        private System.Windows.Forms.Label lblAdults;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblArrival;
        private System.Windows.Forms.GroupBox grpJourney;
        private System.Windows.Forms.GroupBox grpDates;
        private System.Windows.Forms.GroupBox gpPeopleFlying;
        private System.Windows.Forms.ComboBox cmbOrigin;
        private System.Windows.Forms.ComboBox cmbDestination;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblDeparture;
    }
}