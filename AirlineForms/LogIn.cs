﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace AirlineForms
{
    public partial class LogIn : Form
    {
        private readonly int id;
        private int mov;
        private int movX;
        private int movY;

        public LogIn()
        {
            id = id;
            InitializeComponent();
            lblWrongPword.Hide();
            lblNewUser.ForeColor = Color.Gray;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text.Length == 0 || txtPassword.Text.Length == 0)
            {
                MessageBox.Show("Username and password must not be empty.");
            }
            else
            {
                //Creates a connection to the existing database in the program directory
                var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
                var db = new Database(dbConnection);

                var userExists = false;
                try
                {
                    foreach (var user in db.users)
                        if (user.username == txtUsername.Text)
                            userExists = true;
                }

                catch (SQLiteException exception)
                {
                    //If a database file doesn't exist, a new one is made with a 'Users' and 'Flights' table
                    Console.WriteLine(exception);
                    db.ExecuteCommand(@"create table Users
(
    id       integer not null
        primary key,
    username text    not null,
    password text    not null
);

create table Flights
(
    id         integer not null
        constraint Flights_pk
            primary key,
    userid     integer
        references Users,
    date       text,
    flightdata text
);");
                }

                if (userExists)
                {
                    //Hashes the input password
                    var sha256 = SHA256.Create();
                    var passHash =
                        Encoding.ASCII.GetString(sha256.ComputeHash(Encoding.ASCII.GetBytes(txtPassword.Text)));

                    //Checks for a user with the corresponding username and password
                    var userQuery =
                        from user in db.users
                        where user.username == txtUsername.Text && user.password == passHash
                        select user;

                    if (userQuery.Any())
                    {
                        Hide();
                        var ldg = new Landing(userQuery.ToList().First().id.Value);
                        ldg.Show();
                        ldg.FormClosed += (s, args) => Show();
                    }
                    else
                    {
                        lblWrongPword.Show();
                    }
                }
                else
                {
                    lblNewUser.ForeColor = Color.Red;
                    lblNewUser.Text = "New user detected. Enter your \r\ncredentials and press 'Log In'";
                    var sha256 = SHA256.Create();
                    var passHash =
                        Encoding.ASCII.GetString(sha256.ComputeHash(Encoding.ASCII.GetBytes(txtPassword.Text)));
                    var newUser = new user
                    {
                        password = passHash,
                        username = txtUsername.Text
                    };

                    db.users.InsertOnSubmit(newUser);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (SQLiteException exception)
                    {
                        db.SubmitChanges();
                    }
                }
            }
        }

        private void LogIn_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btnLogin_Click(this, new EventArgs());
        }

        private void chbShowPass_CheckedChanged(object sender, EventArgs e)
        {
            txtPassword.UseSystemPasswordChar = !txtPassword.UseSystemPasswordChar;
        }


        private void lblExit_MouseClick(object sender, MouseEventArgs e)
        {
            Application.Exit();
        }

        private void lblExit_MouseEnter(object sender, EventArgs e)
        {
            lblExit.ForeColor = Color.Red;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            lblExit.ForeColor = Color.Black;
        }

        private void pnlDrag_MouseDown(object sender, MouseEventArgs e)
        {
            mov = 1;
            movX = e.X;
            movY = e.Y;
        }

        private void pnlDrag_MouseUp(object sender, MouseEventArgs e)
        {
            mov = 0;
        }

        private void pnlDrag_MouseMove(object sender, MouseEventArgs e)
        {
            if (mov == 1) SetDesktopLocation(MousePosition.X - movX, MousePosition.Y - movY);
        }
    }
}