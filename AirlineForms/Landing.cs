﻿using System;
using System.Windows.Forms;

namespace AirlineForms
{
    public partial class Landing : Form
    {
        private readonly int id;

        public Landing(int id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void btnJourney_Click(object sender, EventArgs e)
        {
            Hide();
            var airTicket = new AirTicket(id);
            airTicket.Show();
            airTicket.Closed += (s, args) => Show();
        }

        private void btnViewFlights_Click(object sender, EventArgs e)
        {
            Hide();
            var upcomingFlights = new UpcomingFlights(id);
            upcomingFlights.Show();
            upcomingFlights.Closed += (s, args) => Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}