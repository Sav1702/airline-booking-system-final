﻿using System.Collections.Generic;

namespace AirlineForms
{
    public class FlightResponse
    {
        public string SessionKey { get; set; }
        public string Status { get; set; }
        public Query Query { get; set; }
        public List<Itineraries> Itineraries { get; set; }
        public List<Legs> Legs { get; set; }
        public List<Segments> Segments { get; set; }
        public List<Carriers> Carriers { get; set; }
        public List<Agents> Agents { get; set; }
        public List<Places> Places { get; set; }
        public List<Currencies> Currencies { get; set; }
    }

    public class Query
    {
        public string Country { get; set; }
        public string Currency { get; set; }
        public string Locale { get; set; }
        public int Adults { get; set; }
        public int Children { get; set; }
        public int Infants { get; set; }
        public string OriginPlace { get; set; }
        public string DestinationPlace { get; set; }
        public string OutboundDate { get; set; }
        public string InboundDate { get; set; }
        public string LocationSchema { get; set; }
        public string CabinClass { get; set; }
        public bool GroupPricing { get; set; }
    }

    public class Itineraries
    {
        public string OutboundLegId { get; set; }
        public string InboundLegId { get; set; }
        public List<PricingOption> PricingOptions { get; set; }
        public BookingDetailsLink BookingDetailsLink { get; set; }
    }

    public class PricingOption
    {
        public List<int> Agents { get; set; }
        public int QuoteAgeInMinutes { get; set; }
        public float Price { get; set; }
        public string DeeplinkUrl { get; set; }
    }

    public class BookingDetailsLink
    {
        public string Uri { get; set; }
        public string Body { get; set; }
        public string Method { get; set; }
    }

    public class Legs
    {
        public string Id { get; set; }
        public List<int> SegmentIds { get; set; }
        public string OriginStation { get; set; }
        public string DestinationStation { get; set; }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public int Duration { get; set; }
        public string JourneyMode { get; set; }
        public List<int> Stops { get; set; }
        public List<int> Carriers { get; set; }
        public List<int> OperatingCarriers { get; set; }
        public string Directionality { get; set; }
        public List<FlightNumbers> FlightNumbers { get; set; }
    }

    public class FlightNumbers
    {
        public string FlightNumber { get; set; }
        public int CarrierId { get; set; }
    }

    public class Segments
    {
        public int Id { get; set; }
        public int OriginStation { get; set; }
        public int DestinationStation { get; set; }
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public int Carrier { get; set; }
        public int OperatingCarrier { get; set; }
        public int Duration { get; set; }
        public string FlightNumber { get; set; }
        public string JourneyMode { get; set; }
        public string Directionality { get; set; }
    }

    public class Carriers
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string DisplayCode { get; set; }
    }

    public class Agents
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Status { get; set; }
        public bool OptimisedForMobile { get; set; }
        public string TravelAgent { get; set; }
    }

    public class Places
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Code { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
    }

    public class Currencies
    {
        public string Code { get; set; }
        public string Symbol { get; set; }
        public string ThousandsSeparator { get; set; }
        public string DecimalSeparator { get; set; }
        public bool SymbolOnLeft { get; set; }
        public bool SpaceBetweenAmountAndSymbol { get; set; }
        public int RoundingCoefficient { get; set; }
        public int DecimalDigits { get; set; }
    }
}