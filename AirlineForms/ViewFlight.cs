﻿using System;
using System.Data.SQLite;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace AirlineForms
{
    public partial class ViewFlight : Form
    {
        private readonly string deepLink;
        private readonly int id;
        private readonly BookingInfo selectedFlight;

        public ViewFlight(int id, BookingInfo selectedFlight, string deepLink)
        {
            InitializeComponent();
            this.id = id;
            this.selectedFlight = selectedFlight;
            this.deepLink = deepLink;
            selectedFlight.deepLink = deepLink;

            if (deepLink == "") btnBookFlight.Visible = false;

            if (selectedFlight.bookingLegs.Count == 2)
            {
                pnlInbound.Visible = true;
                lblInbound.Visible = true;
                lblDateArrive.Visible = true;
                pbInbound.Visible = true;

                loadOutboundData();
                loadInboundData();
            }
            else
            {
                loadOutboundData();
                Size = new Size(888, 417);
                btnBookFlight.Location = new Point(734, 340);
                btnBack.Location = new Point(23, 340);
            }
        }

        public string getAirportCode(int index)
        {
            foreach (var i in selectedFlight.bookingPlaces)
                if (i.Id == index)
                    return i.Code;
            return null;
        }

        private void btnBookFlight_Click(object sender, EventArgs e)
        {
            Process.Start(selectedFlight.deepLink);

            var selectedFlightNoBitmap = new BookingInfo();

            selectedFlightNoBitmap.bookingLegs = selectedFlight.bookingLegs;

            selectedFlightNoBitmap.bookingCarriers = selectedFlight.bookingCarriers;

            selectedFlightNoBitmap.bookingPlaces = selectedFlight.bookingPlaces;

            selectedFlightNoBitmap.deepLink = selectedFlight.deepLink;

            selectedFlightNoBitmap.OriginStation = selectedFlight.OriginStation;

            selectedFlightNoBitmap.DestinationStation = selectedFlight.DestinationStation;

            var flightData = JsonConvert.SerializeObject(selectedFlightNoBitmap);

            var newFlightData = new flight
            {
                userid = id,
                date = DateTime.Today.Date.ToString(),
                flightdata = flightData
            };

            var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
            var db = new Database(dbConnection);

            db.flights.InsertOnSubmit(newFlightData);
            try
            {
                db.SubmitChanges();
            }
            catch (SQLiteException exception)
            {
                db.SubmitChanges();
            }
        }

        private void loadOutboundData()
        {
            lblOutboundDepartCode.Text = getAirportCode(int.Parse(selectedFlight.bookingLegs[0].OriginStation));
            lblOutboundArriveCode.Text = getAirportCode(int.Parse(selectedFlight.bookingLegs[0].DestinationStation));

            lblDateLeave.Text = selectedFlight.bookingLegs[0].Departure.Substring(0, 10);

            lblOutboundTimeLeave.Text = selectedFlight.bookingLegs[0].Departure.Substring(11, 5);
            lblOutboundTimeArrive.Text = selectedFlight.bookingLegs[0].Arrival.Substring(11, 5);

            if (deepLink != "") pbOutbound.Image = selectedFlight.outboundBitmap;
        }

        private void loadInboundData()
        {
            lblInboundDepartCode.Text = getAirportCode(int.Parse(selectedFlight.bookingLegs[1].OriginStation));
            lblInboundArriveCode.Text = getAirportCode(int.Parse(selectedFlight.bookingLegs[1].DestinationStation));

            lblDateArrive.Text = selectedFlight.bookingLegs[1].Departure.Substring(0, 10);

            lblInboundTimeLeave.Text = selectedFlight.bookingLegs[1].Departure.Substring(11, 5);
            lblInboundTimeArrive.Text = selectedFlight.bookingLegs[1].Arrival.Substring(11, 5);

            if (deepLink == "") pbInbound.Image = selectedFlight.inboundBitmap;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}