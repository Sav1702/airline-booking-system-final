﻿namespace AirlineForms
{
    partial class Landing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(AirlineForms.Landing));
            this.btnJourney = new System.Windows.Forms.Button();
            this.btnViewFlights = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblUpcomingFlights = new System.Windows.Forms.Label();
            this.lblJourney = new System.Windows.Forms.Label();
            this.SuspendLayout();
            this.btnJourney.Location = new System.Drawing.Point(31, 69);
            this.btnJourney.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnJourney.Name = "btnJourney";
            this.btnJourney.Size = new System.Drawing.Size(200, 200);
            this.btnJourney.TabIndex = 0;
            this.btnJourney.Text = "Book a Journey";
            this.btnJourney.UseVisualStyleBackColor = true;
            this.btnJourney.Click += new System.EventHandler(this.btnJourney_Click);
            this.btnViewFlights.Location = new System.Drawing.Point(354, 69);
            this.btnViewFlights.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnViewFlights.Name = "btnViewFlights";
            this.btnViewFlights.Size = new System.Drawing.Size(200, 200);
            this.btnViewFlights.TabIndex = 1;
            this.btnViewFlights.Text = "View your upcoming flights";
            this.btnViewFlights.UseVisualStyleBackColor = true;
            this.btnViewFlights.Click += new System.EventHandler(this.btnViewFlights_Click);
            this.btnBack.Location = new System.Drawing.Point(252, 377);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(84, 32);
            this.btnBack.TabIndex = 2;
            this.btnBack.Text = "🠘 Go Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            this.lblUpcomingFlights.Location = new System.Drawing.Point(396, 284);
            this.lblUpcomingFlights.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpcomingFlights.Name = "lblUpcomingFlights";
            this.lblUpcomingFlights.Size = new System.Drawing.Size(119, 38);
            this.lblUpcomingFlights.TabIndex = 3;
            this.lblUpcomingFlights.Text = "View your previously\r\nbooked flights";
            this.lblUpcomingFlights.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblJourney.Location = new System.Drawing.Point(94, 284);
            this.lblJourney.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJourney.Name = "lblJourney";
            this.lblJourney.Size = new System.Drawing.Size(66, 38);
            this.lblJourney.TabIndex = 4;
            this.lblJourney.Text = "Book a \r\nnew flight";
            this.lblJourney.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 421);
            this.Controls.Add(this.lblJourney);
            this.Controls.Add(this.lblUpcomingFlights);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnViewFlights);
            this.Controls.Add(this.btnJourney);
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Landing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button btnJourney;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblUpcomingFlights;
        private System.Windows.Forms.Label lblJourney;
        private System.Windows.Forms.Button btnViewFlights;
    }
}