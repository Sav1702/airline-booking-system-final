﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace AirlineForms
{
    public partial class UpcomingFlights : Form
    {
        private readonly int id;
        private readonly List<BookingInfo> userflights = new List<BookingInfo>();
        private string destination;
        private string destinationStation;

        public UpcomingFlights(int id)
        {
            this.id = id;
            InitializeComponent();
            loadFlights();
        }

        private void loadFlights()
        {
            //object sender, EventArgs e
            var dbConnection = new SQLiteConnection("Data Source=Database.db;Version=3;");
            var db = new Database(dbConnection);

            var flightsQuery =
                from flight in db.flights
                where flight.userid == id
                select flight;

            foreach (var i in flightsQuery.ToList())
            {
                userflights.Add(JsonConvert.DeserializeObject<BookingInfo>(i.flightdata));
                Debug.Print(i.flightdata);
            }


            var count = 0;


            foreach (var j in userflights)
            foreach (var k in j.bookingPlaces)
                if (k.Id.ToString() == j.DestinationStation)
                {
                    dgUpcomingFlights.Rows.Add("Trip to " + k.Name,
                        Convert.ToDateTime(j.bookingLegs[0].Departure.Substring(0, 10)).ToString("dd/MM/yyyy"), count);
                    count++;
                }
        }

        private void dgUpcomingFlights_CellContentClick(object sender, DataGridViewCellEventArgs e)

        {
            var sendergrid = (DataGridView) sender;
            if (sendergrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                var classIndex = dgUpcomingFlights.Rows[e.RowIndex].Cells[2].Value.ToString();

                Hide();
                var viewFlight = new ViewFlight(id, userflights[int.Parse(classIndex)], "");
                viewFlight.Show();
                viewFlight.Closed += (s, args) => Show();
                viewFlight.Closed += (s, args) => Show();
            }
        }

        private void btnBack_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}