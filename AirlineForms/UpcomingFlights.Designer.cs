﻿namespace AirlineForms
{
    partial class UpcomingFlights
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 =
                new System.Windows.Forms.DataGridViewCellStyle();
            this.dgUpcomingFlights = new System.Windows.Forms.DataGridView();
            this.btnBack = new System.Windows.Forms.Button();
            this.FinalDestination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Departure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ViewUpcomingFlight = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize) (this.dgUpcomingFlights)).BeginInit();
            this.SuspendLayout();
            // 
            // dgUpcomingFlights
            // 
            this.dgUpcomingFlights.AllowUserToAddRows = false;
            this.dgUpcomingFlights.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUpcomingFlights.BackgroundColor = System.Drawing.Color.White;
            this.dgUpcomingFlights.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[]
                {this.FinalDestination, this.Departure, this.Index, this.ViewUpcomingFlight});
            this.dgUpcomingFlights.Location = new System.Drawing.Point(0, 33);
            this.dgUpcomingFlights.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgUpcomingFlights.Name = "dgUpcomingFlights";
            this.dgUpcomingFlights.RowHeadersVisible = false;
            this.dgUpcomingFlights.Size = new System.Drawing.Size(1059, 422);
            this.dgUpcomingFlights.TabIndex = 0;
            this.dgUpcomingFlights.CellContentClick +=
                new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUpcomingFlights_CellContentClick);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(0, 1);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(132, 32);
            this.btnBack.TabIndex = 1;
            this.btnBack.Text = "🠘 Go Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click_1);
            // 
            // FinalDestination
            // 
            this.FinalDestination.HeaderText = "Final Destination";
            this.FinalDestination.Name = "FinalDestination";
            this.FinalDestination.ReadOnly = true;
            // 
            // Departure
            // 
            this.Departure.HeaderText = "Departure";
            this.Departure.Name = "Departure";
            this.Departure.ReadOnly = true;
            // 
            // Index
            // 
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Visible = false;
            // 
            // ViewUpcomingFlight
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.MediumOrchid;
            this.ViewUpcomingFlight.DefaultCellStyle = dataGridViewCellStyle1;
            this.ViewUpcomingFlight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ViewUpcomingFlight.HeaderText = "ViewUpcomingFlight";
            this.ViewUpcomingFlight.Name = "ViewUpcomingFlight";
            this.ViewUpcomingFlight.ReadOnly = true;
            this.ViewUpcomingFlight.Text = "View this flight";
            // 
            // UpcomingFlights
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1059, 456);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.dgUpcomingFlights);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "UpcomingFlights";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Your Upcoming Flights";
            ((System.ComponentModel.ISupportInitialize) (this.dgUpcomingFlights)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.DataGridView dgUpcomingFlights;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinalDestination;
        private System.Windows.Forms.DataGridViewTextBoxColumn Departure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewButtonColumn ViewUpcomingFlight;
        private System.Windows.Forms.Button btnBack;
    }
}