﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using RestSharp;

namespace AirlineForms
{
    public partial class AirTicket : Form
    {
        private readonly int id;
        private readonly Segments segmentLoad = new Segments();
        private Carriers carriersLoad = new Carriers();
        private string deepLink;
        private BookingLegs legLoad = new BookingLegs();
        private BookingLegs legLoadReturn = new BookingLegs();
        private string location;
        private IRestResponse<FlightResponse> pollResponse;
        private BookingInfo selectedFlight = new BookingInfo();
        private string selectedFlightIndex;


        public AirTicket(int id)
        {
            this.id = id;
            InitializeComponent();
            cmbAdults.Text = "1";
            cmbChildren.Text = "0";
            cmbInfants.Text = "0";
            dgResponse.RowTemplate.Height = 145;
            dgResponse.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dateLeave.Value = DateTime.Today;
            dateLeave.MinDate = DateTime.Today;
            dateReturn.MinDate = dateLeave.Value;
        }

        private void onewayFlight()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 |
                                                   SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //Creates a session to search for flights using the inputs the user has chosen
            var client =
                new RestClient(
                    "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/pricing/v1.0");
            var request = new RestRequest(Method.POST);
            request.AddHeader("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", "42bcef5741msh882bcaeb9c84cf4p1e30d9jsn1b2c06525803");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");

            //Creates the URL to feed the API which has concatenated all the values the user has chosen
            var responseParam = "cabinClass=" + cmbCabinClass.Text +
                                "&children=" + cmbChildren.Text + "&infants=" +
                                cmbInfants.Text +
                                "&country=GB&currency=GBP&locale=en-GB&originPlace=" + cmbOrigin.Text +
                                "-sky&destinationPlace=" + cmbDestination.Text + "-sky&outboundDate=" +
                                dateLeave.Value.ToString("yyyy-MM-dd") + "&adults=" + cmbAdults.Text;

            request.AddParameter("application/x-www-form-urlencoded", responseParam, ParameterType.RequestBody);
            var response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                if (response.Content.Contains("Rate limit")) lblRateLimit.Visible = true;
                if (response.Content.Contains("Incorrect value") && response.Content.Contains("OriginPlace"))
                    lblOriginError.Visible = true;

                if (response.Content.Contains("Incorrect value") && response.Content.Contains("DestinationPlace"))
                    lblDestinationError.Visible = true;

                if (response.Content.Contains("Date in the past") && response.Content.Contains("OutboundDate"))
                    lblLeaveError.Visible = true;

                if (response.Content.Contains("Date in the past") && response.Content.Contains("InboundDate"))
                    lblReturnError.Visible = true;

                if (response.Content.Contains("CabinClass"))
                    lblCabinError.Visible = true;
            }
            else
            {
                //Obtains the session key from the response header for polling results
                location = response.Headers[7].ToString().Substring(73);

                //Polling results using the key from the previous "create session" command
                var pollClient = new RestClient("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");
                var pollRequest = new RestRequest("/apiservices/pricing/uk2/v1.0/{location}", Method.GET);
                pollRequest.AddUrlSegment("location", location);
                pollRequest.AddHeader("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");
                pollRequest.AddHeader("x-rapidapi-key", "42bcef5741msh882bcaeb9c84cf4p1e30d9jsn1b2c06525803");

                //Deserialises the response into the "FlightResponse" rootobject in my class structure
                pollResponse = pollClient.Execute<FlightResponse>(pollRequest);

                if (!pollResponse.IsSuccessful)
                {
                    if (pollResponse.Content.Contains("Rate limit has been exceeded")) lblRateLimit.Visible = true;
                }
                else if (pollResponse.Data.Itineraries.Count == 0)
                {
                    MessageBox.Show("Sorry, no flights are available for the selected route and cabin class.");
                }

                else
                {
                    dgResponse.Columns[0].CellTemplate.ValueType = typeof(Bitmap);
                    var newRow = new List<object>();

                    var count = 0;
                    while (count < pollResponse.Data.Itineraries.Count && count < 35)
                    {
                        var bmpOutbound = new Bitmap(getOutboundCarrierBitmap(count));

                        newRow.Add(bmpOutbound);
                        newRow.Add(count.ToString());
                        newRow.Add(getOriginCode() + "➡" + getDestinationCode() + Environment.NewLine +
                                   getDepartureTime(count) + "    " + getArrivalTime(count));
                        newRow.Add(pollResponse.Data.Currencies[0].Symbol +
                                   pollResponse.Data.Itineraries[count].PricingOptions[0].Price);

                        dgResponse.Rows.Add(newRow[0], newRow[1], newRow[2], newRow[3]);
                        count++;
                        newRow.Clear();
                    }
                }
            }
        }

        private void returnFlight()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 |
                                                   SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //(STAGE1) Creates a session to search for flights using the inputs the user has chosen
            var client =
                new RestClient(
                    "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/pricing/v1.0");
            var request = new RestRequest(Method.POST);
            request.AddHeader("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");
            request.AddHeader("x-rapidapi-key", "42bcef5741msh882bcaeb9c84cf4p1e30d9jsn1b2c06525803");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");

            //Creates the URL to feed the API which has concatenated all the values the user has chosen
            var responseParam = "inboundDate=" + dateReturn.Value.ToString("yyyy-MM-dd") +
                                "&cabinClass=" + cmbCabinClass.Text + "&children=" + cmbChildren.Text + "&infants=" +
                                cmbInfants.Text +
                                "&groupPricing=true&country=GB&currency=GBP&locale=en-GB&originPlace=" +
                                cmbOrigin.Text + "-sky&destinationPlace=" + cmbDestination.Text + "-sky&outboundDate=" +
                                dateLeave.Value.ToString("yyyy-MM-dd") + "&adults=" + cmbAdults.Text;

            //Executes the request
            request.AddParameter("application/x-www-form-urlencoded", responseParam, ParameterType.RequestBody);
            var response = client.Execute(request);


            if (!response.IsSuccessful)
            {
                if (response.Content.Contains("Rate limit")) lblRateLimit.Visible = true;
                if (response.Content.Contains("Incorrect value") && response.Content.Contains("OriginPlace"))
                    lblOriginError.Visible = true;

                if (response.Content.Contains("Incorrect value") && response.Content.Contains("DestinationPlace"))
                    lblDestinationError.Visible = true;

                if (response.Content.Contains("Date in the past") && response.Content.Contains("OutboundDate"))
                    lblLeaveError.Visible = true;

                if (response.Content.Contains("Date in the past") && response.Content.Contains("InboundDate"))
                    lblReturnError.Visible = true;

                if (response.Content.Contains("CabinClass"))
                    lblCabinError.Visible = true;
            }


            else
            {
                //Obtains the session key from the response header for polling results
                location = response.Headers[7].ToString().Substring(73);

                //Polling results using the key from the previous "create session" command
                var pollClient = new RestClient("https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");
                var pollRequest = new RestRequest("/apiservices/pricing/uk2/v1.0/{location}", Method.GET);
                pollRequest.AddUrlSegment("location", location);
                pollRequest.AddHeader("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com");
                pollRequest.AddHeader("x-rapidapi-key", "42bcef5741msh882bcaeb9c84cf4p1e30d9jsn1b2c06525803");
                var response2 = client.Execute(pollRequest);

                //Deserialises the response into the "FlightResponse" rootobject in my class structure
                pollResponse = pollClient.Execute<FlightResponse>(pollRequest);

                if (!pollResponse.IsSuccessful)
                {
                    if (pollResponse.Content.Contains("Rate limit has been exceeded")) lblRateLimit.Visible = true;
                }

                else
                {
                    dgResponse.Columns[0].CellTemplate.ValueType = typeof(Bitmap);
                    var newRow = new List<object>();

                    var count = 0;
                    while (count < pollResponse.Data.Itineraries.Count && count < 35)
                    {
                        var bmpOutbound = new Bitmap(getOutboundCarrierBitmap(count));
                        var bmpInbound = new Bitmap(getInboundCarrierBitmap(count));

                        var bmpJourney = new Bitmap(Math.Max(bmpOutbound.Width, bmpInbound.Width),
                            bmpOutbound.Height + bmpInbound.Height);
                        var gr = Graphics.FromImage(bmpJourney);
                        gr.DrawImage(bmpOutbound, 0, 0, bmpOutbound.Width, bmpOutbound.Height);
                        gr.DrawImage(bmpInbound, 0, bmpOutbound.Height);
                        bmpJourney.Save("fullImage");

                        newRow.Add(bmpJourney);
                        newRow.Add(count.ToString());
                        newRow.Add(getOriginCode() + "➡" + getDestinationCode());
                        newRow.Add(pollResponse.Data.Currencies[0].Symbol +
                                   pollResponse.Data.Itineraries[count].PricingOptions[0].Price);

                        dgResponse.Rows.Add(newRow[0], newRow[1], newRow[2], newRow[3]);
                        count++;
                        newRow.Clear();
                    }
                }
            }
        }


        public void sortList()
        {
            var rowlist = new List<DataGridViewRow>();
            foreach (DataGridViewRow row in dgResponse.Rows) rowlist.Add(row);
            rowlist = mergeSort(rowlist);
            dgResponse.Rows.Clear();
            foreach (var row in rowlist) dgResponse.Rows.Add(row);
        }

        //Function that returns the name of the origin airport
        public string getOriginName()
        {
            var origin = pollResponse.Data.Legs[1].OriginStation;
            foreach (var i in pollResponse.Data.Places
            ) //iterates through the places list to find the matching name for the id
                if (origin == i.Id.ToString())
                    return i.Name;
            return null;
        }

        //Function that returns the name of the destination airport
        //public string getDestinationName()
        //{
        //    var destination = pollResponse.Data.Legs[1].DestinationStation;
        //    foreach (var i in pollResponse.Data.Places)
        //        if (destination == i.Id.ToString())
        //            return i.Name;
        //    return null;
        //}

        //Function that returns the IATA code of the origin airport
        public string getOriginCode()
        {
            var origin = pollResponse.Data.Legs[1].OriginStation;
            foreach (var i in pollResponse.Data.Places)
                if (origin == i.Id.ToString())
                    return i.Code;
            return null;
        }

        //Function that returns the IATA code of the destination airport
        public string getDestinationCode()
        {
            var destination = pollResponse.Data.Legs[1].DestinationStation;
            foreach (var i in pollResponse.Data.Places)
                if (destination == i.Id.ToString())
                    return i.Code;
            return null;
        }

        public string getDepartureTime(int index) //IMPROVE
        {
            var outBoundLegId = pollResponse.Data.Itineraries[index].OutboundLegId;
            foreach (var i in pollResponse.Data.Legs)
                if (i.Id == outBoundLegId)
                    return i.Departure.Substring(11, 5);
            return null;
        }

        public string getArrivalTime(int index) //IMPROVE
        {
            var outBoundLegId = pollResponse.Data.Itineraries[index].OutboundLegId;
            foreach (var i in pollResponse.Data.Legs)
                if (i.Id == outBoundLegId)
                    return i.Arrival.Substring(11, 5);
            return null;
        }

        //Function that returns a bitmap of the carrier logo at a specific index
        public Bitmap getInboundCarrierBitmap(int index)
        {
            var carrierID = 0;
            foreach (var i in pollResponse.Data.Legs)
                if (i.Id == pollResponse.Data.Itineraries[index].InboundLegId)
                {
                    carrierID = i.Carriers[0];
                    break;
                }

            foreach (var i in pollResponse.Data.Carriers)
                if (carrierID == i.Id)
                {
                    var url = i.ImageUrl;
                    var myRequest = (HttpWebRequest) WebRequest.Create(url);
                    myRequest.Method = "GET";
                    var myResponse = (HttpWebResponse) myRequest.GetResponse();
                    var Carrierbmp = new Bitmap(myResponse.GetResponseStream());
                    myResponse.Close();
                    return Carrierbmp;
                }

            return null;
        }

        public Bitmap getOutboundCarrierBitmap(int index)
        {
            var carrierID = 0;
            foreach (var i in pollResponse.Data.Legs)
                if (i.Id == pollResponse.Data.Itineraries[index].OutboundLegId)
                {
                    carrierID = i.Carriers[0];
                    break;
                }

            foreach (var i in pollResponse.Data.Carriers)
                if (carrierID == i.Id)
                {
                    var url = i.ImageUrl;
                    var myRequest = (HttpWebRequest) WebRequest.Create(url);
                    myRequest.Method = "GET";
                    var myResponse = (HttpWebResponse) myRequest.GetResponse();
                    var Carrierbmp = new Bitmap(myResponse.GetResponseStream());
                    myResponse.Close();
                    return Carrierbmp;
                }

            return null;
        }

        private static List<DataGridViewRow> mergeSort(List<DataGridViewRow> unsorted)
        {
            if (unsorted.Count <= 1)
                return unsorted;

            var left = new List<DataGridViewRow>();
            var right = new List<DataGridViewRow>();

            var middle = unsorted.Count / 2;
            for (var i = 0; i < middle; i++) left.Add(unsorted[i]);
            for (var i = middle; i < unsorted.Count; i++) right.Add(unsorted[i]);

            //Recursion is utilised to repeatedly split the list until a base case is reached
            left = mergeSort(left);
            right = mergeSort(right);

            //Base case of starting to return the merged lists
            return Merge(left, right);
        }

        private static List<DataGridViewRow> Merge(List<DataGridViewRow> left, List<DataGridViewRow> right)
        {
            //Defines the list as a list of rows, so the integer values need to be extracted from a column
            var result = new List<DataGridViewRow>();

            while (left.Count > 0 || right.Count > 0)
            {
                Debug.Print(left.Count.ToString());
                Debug.Print(right.Count.ToString());
                if (left.Count > 0 && right.Count > 0)
                {
                    //Compares integers from the 'Price' column and ignores the currency symbol
                    //Depending on the result of the statement below, the list split into left and right
                    if (decimal.Parse((string) left.First().Cells.GetCellValueFromColumnHeader("Price"),
                            NumberStyles.AllowCurrencySymbol | NumberStyles.Number) <= decimal.Parse(
                            (string) right.First().Cells.GetCellValueFromColumnHeader("Price"),
                            NumberStyles.AllowCurrencySymbol | NumberStyles.Number))
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                    }
                }
                else if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                }
                else if (right.Count > 0)
                {
                    result.Add(right.First());
                    right.Remove(right.First());
                }
            }

            return result;
        }

        private void chbRoundTrip_CheckedChanged(object sender, EventArgs e)
        {
            if (chbRoundTrip.Checked)
                grpDates.Size = new Size(320, 166);
            else
                grpDates.Size = new Size(162, 166);
            dateReturn.Visible = !dateReturn.Visible;
            lblArrival.Visible = !lblArrival.Visible;
        }

        private void btnSearchFlights_Click(object sender, EventArgs e)
        {
            btnSortList.Visible = true;
            dgResponse.Rows.Clear();
            dgResponse.Refresh();
            lblRateLimit.Visible = false;

            if (chbRoundTrip.Checked)
                returnFlight();
            else
                onewayFlight();
        }

        private void cmbOrigin_TextChanged(object sender, EventArgs e)
        {
            lblOriginError.Visible = false;
        }

        private void cmbDestination_TextChanged(object sender, EventArgs e)
        {
            lblDestinationError.Visible = false;
        }

        private void dateLeave_ValueChanged(object sender, EventArgs e)
        {
            lblLeaveError.Visible = false;
            dateReturn.MinDate = dateLeave.Value;
        }

        private void dateReturn_ValueChanged(object sender, EventArgs e)
        {
            lblReturnError.Visible = false;
        }

        private void cmbCabinClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblCabinError.Visible = false;
        }

        private void btnSortList_Click(object sender, EventArgs e)
        {
            if (dgResponse.RowCount == 0)
                MessageBox.Show("No flights to sort. Please try again.");
            else
                sortList();
        }

        private void dgResponse_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var booking1 = new BookingInfo();
            if (e.ColumnIndex == dgResponse.Columns["Select"].Index && e.RowIndex >= 0)
            {
                selectedFlightIndex = dgResponse.Rows[e.RowIndex].Cells[1].Value.ToString();
                deepLink = pollResponse.Data.Itineraries[int.Parse(selectedFlightIndex)].PricingOptions[0].DeeplinkUrl;

                loadBooking();

                Hide();
                var viewFlight = new ViewFlight(id, selectedFlight, deepLink);
                viewFlight.Show();
                viewFlight.Closed += (s, args) => Show();
            }
        }

        private void loadBooking()
        {
            //create code to iterate through legs finding the correct one, and then load that leg into the BookingInfo class structure
            //then check for an inbound and do the same if it is !null
            legLoad = new BookingLegs();
            legLoadReturn = new BookingLegs();
            selectedFlight = new BookingInfo();

            foreach (var i in pollResponse.Data.Legs)
            {
                if (pollResponse.Data.Itineraries[int.Parse(selectedFlightIndex)].OutboundLegId == i.Id)
                {
                    legLoad.Id = i.Id;
                    legLoad.SegmentIds = i.SegmentIds;
                    legLoad.OriginStation = i.OriginStation;
                    legLoad.DestinationStation = i.DestinationStation;
                    legLoad.Departure = i.Departure;
                    legLoad.Arrival = i.Arrival;
                    legLoad.Duration = i.Duration;
                    legLoad.JourneyMode = i.JourneyMode;
                    legLoad.Stops = i.Stops;
                    legLoad.Carriers = i.Carriers;
                    legLoad.OperatingCarriers = i.OperatingCarriers;
                    legLoad.Directionality = i.Directionality;
                    legLoad.FlightNumbers = i.FlightNumbers;

                    selectedFlight.bookingLegs.Add(legLoad);
                }

                if (pollResponse.Data.Itineraries[int.Parse(selectedFlightIndex)].InboundLegId != null &&
                    pollResponse.Data.Itineraries[int.Parse(selectedFlightIndex)].InboundLegId == i.Id)
                {
                    legLoadReturn.Id = i.Id;
                    legLoadReturn.SegmentIds = i.SegmentIds;
                    legLoadReturn.OriginStation = i.OriginStation;
                    legLoadReturn.DestinationStation = i.DestinationStation;
                    legLoadReturn.Departure = i.Departure;
                    legLoadReturn.Arrival = i.Arrival;
                    legLoadReturn.Duration = i.Duration;
                    legLoadReturn.JourneyMode = i.JourneyMode;
                    legLoadReturn.Stops = i.Stops;
                    legLoadReturn.Carriers = i.Carriers;
                    legLoadReturn.OperatingCarriers = i.OperatingCarriers;
                    legLoadReturn.Directionality = i.Directionality;
                    legLoadReturn.FlightNumbers = i.FlightNumbers;

                    selectedFlight.bookingLegs.Add(legLoadReturn);
                }
            }

            foreach (var j in selectedFlight.bookingLegs)
            foreach (var k in j.SegmentIds)
            foreach (var l in pollResponse.Data.Segments)
                if (k == l.Id)
                {
                    //make a new segments class, add all info from the matching segment to that variable and then load the object into the segment list in SelectedFlight#
                    segmentLoad.ArrivalDateTime = l.ArrivalDateTime;
                    segmentLoad.Carrier = l.Carrier;
                    segmentLoad.DepartureDateTime = l.DepartureDateTime;
                    segmentLoad.DestinationStation = l.DestinationStation;
                    segmentLoad.Directionality = l.Directionality;
                    segmentLoad.Duration = l.Duration;
                    segmentLoad.FlightNumber = l.FlightNumber;
                    segmentLoad.Id = l.Id;
                    segmentLoad.JourneyMode = l.JourneyMode;
                    segmentLoad.OperatingCarrier = l.OperatingCarrier;
                    segmentLoad.OriginStation = l.OriginStation;

                    j.segmentList.Add(segmentLoad);
                }

            selectedFlight.bookingCarriers = pollResponse.Data.Carriers;
            selectedFlight.bookingPlaces = pollResponse.Data.Places;
            selectedFlight.OriginStation = pollResponse.Data.Legs[0].OriginStation;
            selectedFlight.DestinationStation = pollResponse.Data.Legs[0].DestinationStation;
            selectedFlight.outboundBitmap = getOutboundCarrierBitmap(int.Parse(selectedFlightIndex));
            selectedFlight.inboundBitmap = getInboundCarrierBitmap(int.Parse(selectedFlightIndex));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(location);
        }


        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AirTicket_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btnSearchFlights_Click(this, new EventArgs());
        }
    }

    public static class DataGridHelper
    {
        public static object GetCellValueFromColumnHeader(this DataGridViewCellCollection CellCollection,
            string HeaderText)
        {
            return CellCollection.Cast<DataGridViewCell>().First(c => c.OwningColumn.HeaderText == HeaderText).Value;
        }
    }
}