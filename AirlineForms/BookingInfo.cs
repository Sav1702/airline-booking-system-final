﻿using System.Collections.Generic;
using System.Drawing;

namespace AirlineForms
{
    public class BookingInfo
    {
        public List<BookingLegs> bookingLegs { get; set; } = new List<BookingLegs>();
        public List<Carriers> bookingCarriers { get; set; } = new List<Carriers>();
        public List<Places> bookingPlaces { get; set; } = new List<Places>();

        public string OriginStation { get; set; }
        public string DestinationStation { get; set; }
        public string deepLink { get; set; }

        public Bitmap outboundBitmap { get; set; }
        public Bitmap inboundBitmap { get; set; }
    }

    public class BookingLegs : Legs
    {
        public List<Segments> segmentList { get; set; } = new List<Segments>();
    }
}