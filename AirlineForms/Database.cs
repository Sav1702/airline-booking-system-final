﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineForms
{
    [Table(Name = "Users")]
    public class user
    {
        [Column(IsPrimaryKey = true)]
        public int? id { get; set; }

        [Column]
        public string username { get; set; }

        [Column]
        public string password { get; set; }

    }

    [Table(Name = "Flights")]
    public class flight
    {
        [Column(IsPrimaryKey = true)]
        public int? id { get; set; }

        [Column]
        public int userid { get; set; }

        [Column]
        public string date { get; set; }

        [Column]
        public string flightdata { get; set; }
    }


    [Database]
    public partial class Database : DataContext
    {
        public Table<user> users;
        public Table<flight> flights;


        public Database(SQLiteConnection connection) : base(connection) { }
    }
}

