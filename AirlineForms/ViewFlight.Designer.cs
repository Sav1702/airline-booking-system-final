﻿namespace AirlineForms
{
    partial class ViewFlight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(AirlineForms.ViewFlight));
            this.lblOutbound = new System.Windows.Forms.Label();
            this.lblDateLeave = new System.Windows.Forms.Label();
            this.lblOutboundTimeLeave = new System.Windows.Forms.Label();
            this.lblOutboundDepartCode = new System.Windows.Forms.Label();
            this.pbOutboundArrow = new System.Windows.Forms.PictureBox();
            this.lblOutboundTimeArrive = new System.Windows.Forms.Label();
            this.lblOutboundArriveCode = new System.Windows.Forms.Label();
            this.btnBookFlight = new System.Windows.Forms.Button();
            this.pnlOutbound = new System.Windows.Forms.Panel();
            this.pnlInbound = new System.Windows.Forms.Panel();
            this.lblInboundTimeLeave = new System.Windows.Forms.Label();
            this.lblInboundDepartCode = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblInboundTimeArrive = new System.Windows.Forms.Label();
            this.lblInboundArriveCode = new System.Windows.Forms.Label();
            this.lblDateArrive = new System.Windows.Forms.Label();
            this.lblInbound = new System.Windows.Forms.Label();
            this.pbOutbound = new System.Windows.Forms.PictureBox();
            this.pbInbound = new System.Windows.Forms.PictureBox();
            this.btnBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize) (this.pbOutboundArrow)).BeginInit();
            this.pnlOutbound.SuspendLayout();
            this.pnlInbound.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbOutbound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbInbound)).BeginInit();
            this.SuspendLayout();
            this.lblOutbound.AutoSize = true;
            this.lblOutbound.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblOutbound.Location = new System.Drawing.Point(68, 31);
            this.lblOutbound.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutbound.Name = "lblOutbound";
            this.lblOutbound.Size = new System.Drawing.Size(155, 34);
            this.lblOutbound.TabIndex = 0;
            this.lblOutbound.Text = "Outbound";
            this.lblDateLeave.AutoSize = true;
            this.lblDateLeave.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblDateLeave.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblDateLeave.Location = new System.Drawing.Point(255, 42);
            this.lblDateLeave.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateLeave.Name = "lblDateLeave";
            this.lblDateLeave.Size = new System.Drawing.Size(100, 22);
            this.lblDateLeave.TabIndex = 1;
            this.lblDateLeave.Text = "Date leave";
            this.lblOutboundTimeLeave.AutoSize = true;
            this.lblOutboundTimeLeave.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblOutboundTimeLeave.Location = new System.Drawing.Point(21, 73);
            this.lblOutboundTimeLeave.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutboundTimeLeave.Name = "lblOutboundTimeLeave";
            this.lblOutboundTimeLeave.Size = new System.Drawing.Size(106, 24);
            this.lblOutboundTimeLeave.TabIndex = 2;
            this.lblOutboundTimeLeave.Text = "time leave";
            this.lblOutboundDepartCode.AutoSize = true;
            this.lblOutboundDepartCode.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblOutboundDepartCode.Location = new System.Drawing.Point(19, 115);
            this.lblOutboundDepartCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutboundDepartCode.Name = "lblOutboundDepartCode";
            this.lblOutboundDepartCode.Size = new System.Drawing.Size(192, 36);
            this.lblOutboundDepartCode.TabIndex = 3;
            this.lblOutboundDepartCode.Text = "Depart Code";
            this.pbOutboundArrow.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("pbOutboundArrow.BackgroundImage")));
            this.pbOutboundArrow.InitialImage =
                ((System.Drawing.Image) (resources.GetObject("pbOutboundArrow.InitialImage")));
            this.pbOutboundArrow.Location = new System.Drawing.Point(253, 93);
            this.pbOutboundArrow.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pbOutboundArrow.Name = "pbOutboundArrow";
            this.pbOutboundArrow.Size = new System.Drawing.Size(248, 37);
            this.pbOutboundArrow.TabIndex = 4;
            this.pbOutboundArrow.TabStop = false;
            this.lblOutboundTimeArrive.AutoSize = true;
            this.lblOutboundTimeArrive.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblOutboundTimeArrive.Location = new System.Drawing.Point(547, 73);
            this.lblOutboundTimeArrive.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutboundTimeArrive.Name = "lblOutboundTimeArrive";
            this.lblOutboundTimeArrive.Size = new System.Drawing.Size(109, 24);
            this.lblOutboundTimeArrive.TabIndex = 5;
            this.lblOutboundTimeArrive.Text = "time arrive";
            this.lblOutboundArriveCode.AutoSize = true;
            this.lblOutboundArriveCode.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblOutboundArriveCode.Location = new System.Drawing.Point(545, 115);
            this.lblOutboundArriveCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutboundArriveCode.Name = "lblOutboundArriveCode";
            this.lblOutboundArriveCode.Size = new System.Drawing.Size(180, 36);
            this.lblOutboundArriveCode.TabIndex = 6;
            this.lblOutboundArriveCode.Text = "Arrive Code";
            this.btnBookFlight.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBookFlight.Location = new System.Drawing.Point(734, 684);
            this.btnBookFlight.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnBookFlight.Name = "btnBookFlight";
            this.btnBookFlight.Size = new System.Drawing.Size(113, 31);
            this.btnBookFlight.TabIndex = 7;
            this.btnBookFlight.Text = "Book Flight";
            this.btnBookFlight.UseVisualStyleBackColor = true;
            this.btnBookFlight.Click += new System.EventHandler(this.btnBookFlight_Click);
            this.pnlOutbound.BackColor = System.Drawing.Color.Transparent;
            this.pnlOutbound.Controls.Add(this.lblOutboundTimeLeave);
            this.pnlOutbound.Controls.Add(this.lblOutboundDepartCode);
            this.pnlOutbound.Controls.Add(this.pbOutboundArrow);
            this.pnlOutbound.Controls.Add(this.lblOutboundTimeArrive);
            this.pnlOutbound.Controls.Add(this.lblOutboundArriveCode);
            this.pnlOutbound.Location = new System.Drawing.Point(75, 88);
            this.pnlOutbound.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnlOutbound.Name = "pnlOutbound";
            this.pnlOutbound.Size = new System.Drawing.Size(772, 234);
            this.pnlOutbound.TabIndex = 8;
            this.pnlInbound.Controls.Add(this.lblInboundTimeLeave);
            this.pnlInbound.Controls.Add(this.lblInboundDepartCode);
            this.pnlInbound.Controls.Add(this.pictureBox1);
            this.pnlInbound.Controls.Add(this.lblInboundTimeArrive);
            this.pnlInbound.Controls.Add(this.lblInboundArriveCode);
            this.pnlInbound.Location = new System.Drawing.Point(75, 435);
            this.pnlInbound.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pnlInbound.Name = "pnlInbound";
            this.pnlInbound.Size = new System.Drawing.Size(772, 234);
            this.pnlInbound.TabIndex = 9;
            this.pnlInbound.Visible = false;
            this.lblInboundTimeLeave.AutoSize = true;
            this.lblInboundTimeLeave.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblInboundTimeLeave.Location = new System.Drawing.Point(21, 73);
            this.lblInboundTimeLeave.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInboundTimeLeave.Name = "lblInboundTimeLeave";
            this.lblInboundTimeLeave.Size = new System.Drawing.Size(106, 24);
            this.lblInboundTimeLeave.TabIndex = 2;
            this.lblInboundTimeLeave.Text = "time leave";
            this.lblInboundDepartCode.AutoSize = true;
            this.lblInboundDepartCode.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblInboundDepartCode.Location = new System.Drawing.Point(19, 115);
            this.lblInboundDepartCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInboundDepartCode.Name = "lblInboundDepartCode";
            this.lblInboundDepartCode.Size = new System.Drawing.Size(192, 36);
            this.lblInboundDepartCode.TabIndex = 3;
            this.lblInboundDepartCode.Text = "Depart Code";
            this.pictureBox1.BackgroundImage =
                ((System.Drawing.Image) (resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image) (resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(253, 93);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(248, 37);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.lblInboundTimeArrive.AutoSize = true;
            this.lblInboundTimeArrive.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblInboundTimeArrive.Location = new System.Drawing.Point(547, 73);
            this.lblInboundTimeArrive.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInboundTimeArrive.Name = "lblInboundTimeArrive";
            this.lblInboundTimeArrive.Size = new System.Drawing.Size(109, 24);
            this.lblInboundTimeArrive.TabIndex = 5;
            this.lblInboundTimeArrive.Text = "time arrive";
            this.lblInboundArriveCode.AutoSize = true;
            this.lblInboundArriveCode.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblInboundArriveCode.Location = new System.Drawing.Point(545, 115);
            this.lblInboundArriveCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInboundArriveCode.Name = "lblInboundArriveCode";
            this.lblInboundArriveCode.Size = new System.Drawing.Size(180, 36);
            this.lblInboundArriveCode.TabIndex = 6;
            this.lblInboundArriveCode.Text = "Arrive Code";
            this.lblDateArrive.AutoSize = true;
            this.lblDateArrive.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblDateArrive.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblDateArrive.Location = new System.Drawing.Point(227, 380);
            this.lblDateArrive.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateArrive.Name = "lblDateArrive";
            this.lblDateArrive.Size = new System.Drawing.Size(100, 22);
            this.lblDateArrive.TabIndex = 11;
            this.lblDateArrive.Text = "Date leave";
            this.lblDateArrive.Visible = false;
            this.lblInbound.AutoSize = true;
            this.lblInbound.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.lblInbound.Location = new System.Drawing.Point(68, 369);
            this.lblInbound.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInbound.Name = "lblInbound";
            this.lblInbound.Size = new System.Drawing.Size(131, 34);
            this.lblInbound.TabIndex = 10;
            this.lblInbound.Text = "Inbound";
            this.lblInbound.Visible = false;
            this.pbOutbound.Location = new System.Drawing.Point(379, 12);
            this.pbOutbound.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pbOutbound.Name = "pbOutbound";
            this.pbOutbound.Size = new System.Drawing.Size(296, 115);
            this.pbOutbound.TabIndex = 14;
            this.pbOutbound.TabStop = false;
            this.pbInbound.Location = new System.Drawing.Point(379, 352);
            this.pbInbound.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pbInbound.Name = "pbInbound";
            this.pbInbound.Size = new System.Drawing.Size(296, 115);
            this.pbInbound.TabIndex = 15;
            this.pbInbound.TabStop = false;
            this.pbInbound.Visible = false;
            this.btnBack.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBack.Location = new System.Drawing.Point(23, 684);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(113, 31);
            this.btnBack.TabIndex = 16;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(872, 747);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.pbInbound);
            this.Controls.Add(this.pbOutbound);
            this.Controls.Add(this.lblDateArrive);
            this.Controls.Add(this.lblInbound);
            this.Controls.Add(this.pnlInbound);
            this.Controls.Add(this.pnlOutbound);
            this.Controls.Add(this.btnBookFlight);
            this.Controls.Add(this.lblDateLeave);
            this.Controls.Add(this.lblOutbound);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ViewFlight";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Itinerary";
            ((System.ComponentModel.ISupportInitialize) (this.pbOutboundArrow)).EndInit();
            this.pnlOutbound.ResumeLayout(false);
            this.pnlOutbound.PerformLayout();
            this.pnlInbound.ResumeLayout(false);
            this.pnlInbound.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbOutbound)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.pbInbound)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label lblOutbound;
        private System.Windows.Forms.Label lblDateLeave;
        private System.Windows.Forms.Label lblOutboundTimeLeave;
        private System.Windows.Forms.Label lblOutboundDepartCode;
        private System.Windows.Forms.PictureBox pbOutboundArrow;
        private System.Windows.Forms.Label lblOutboundTimeArrive;
        private System.Windows.Forms.Label lblOutboundArriveCode;
        private System.Windows.Forms.Button btnBookFlight;
        private System.Windows.Forms.Panel pnlOutbound;
        private System.Windows.Forms.Panel pnlInbound;
        private System.Windows.Forms.Label lblInboundTimeLeave;
        private System.Windows.Forms.Label lblInboundDepartCode;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblInboundTimeArrive;
        private System.Windows.Forms.Label lblInboundArriveCode;
        private System.Windows.Forms.Label lblDateArrive;
        private System.Windows.Forms.Label lblInbound;
        private System.Windows.Forms.PictureBox pbOutbound;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.PictureBox pbInbound;
    }
}